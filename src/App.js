import React from 'react';
import './App.css';
import Todolist from './todolist/todolist';
import DisplayInput from './displayinput/displayinput';


function App() {
  return (
    //<DisplayInput initText="type somthing..."/>
    <Todolist />
  );
}

export default App;

import React from 'react';
import TextInput from './textinput';
import TextDisplay from './textdisplay';


class DisplayInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }

  handleInput = (e) => {
    this.setState({text: e.target.value});
  }

  render() {
    return (
      <div>
        <TextInput handleInput={this.handleInput} initText="Type something..." />
        <TextDisplay text={this.state.text}/>
      </div>
    );
  }
}

export default DisplayInput;

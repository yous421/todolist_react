import React from 'react';

class InputDisplay extends React.Component {
  render() {
    return (
      <input 
        type="text" 
        placeholder={this.props.initText}
        onChange={this.props.handleInput}
      />
    );
  }
}

export default InputDisplay;

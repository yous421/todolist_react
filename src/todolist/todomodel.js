import Util from './util';


class TodoModel {
  constructor(key) {
    this.key = key;
    this.todos = Util.store(key);
  }

  add(todo) {
    this.todos.push({
      id: new Date().getTime(),
      todo: todo,
      completed: false
    });
    Util.store(this.key, this.todos);
  }

  remove(id) {
    this.todos.map((todo, index) => {
      if(todo.id == id) {
        this.todos.splice(index, 1);
      }
    });
    Util.store(this.key, this.todos);
  }

  removeAll() {
    this.todos = [];
    Util.store(this.key, this.todos);
  }

  check(id) {
    this.todos.map((todo, index) => {
      if(todo.id == id) {
        todo.completed = !todo.completed;
      }
    });
    Util.store(this.key, this.todos);
  }
}

export default TodoModel;

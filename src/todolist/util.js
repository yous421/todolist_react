var util = util || {};

util = {
  store: function (key, data) {
    if (data) {
      return localStorage.setItem(key, JSON.stringify(data));
    }

    var store = localStorage.getItem(key);
    return (store && JSON.parse(store)) || [];
  }
};

export default util;

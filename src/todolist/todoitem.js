import React from 'react';

class TodoItem extends React.Component {
  render() {
    let textStyle = this.props.todo.completed ? {textDecoration: 'line-through'} : {};
    return (
      <li id={this.props.todo.id}>
        <input 
          type="checkbox"
          checked={this.props.todo.completed} 
          onChange={this.props.handleCheck}
        ></input>
        <span style={textStyle}>
          {this.props.todo.todo}
        </span>
        <button onClick={this.props.handleRemove}>X</button>
      </li>
    );
  }
}

export default TodoItem;

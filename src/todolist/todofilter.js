import React from 'react';

class TodoFilter extends React.Component {

  render() {
    let filter = this.props.filter;
    return (
      <div>
        <span 
          style={{
            background: filter === 'all' ? '#7b7b7b' : '',
            border: '1px solid #000000'
          }}           
          defaultChecked={filter === 'all'} 
          onClick={() => this.props.handleFilter('all')}
          >All
        </span>

        <span 
          style={{
            background: filter === 'active' ? '#7b7b7b' : '',
            border: '1px solid #000000'
          }} 
          defaultChecked={filter === 'active'}
          onClick={() => this.props.handleFilter('active')}
          >Active
        </span>

        <span 
          style={{
            background: filter === 'completed' ? '#7b7b7b' : '',
            border: '1px solid #000000'
          }}           
          defaultChecked={filter === 'completed'} 
          onClick={() => this.props.handleFilter('completed')}
          >Completed
        </span>
      </div>
    );
  }
}

export default TodoFilter;

import React from 'react';
import TodoItem from './todoitem';
import TodoModel from './todomodel';
import TodoFilter from './todofilter';


const ENTER_KEY = 13;

class TodoList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      model: [],
      modelFilter: [],
      todoInput: '',
      filter: 'all'
    };
  }

  componentDidMount() {
    var todoModel = new TodoModel('todolist');
    this.setState({model: todoModel});
  }

  handleInput = (e) => {
    this.setState({todoInput: e.target.value});
  }

  handleAdd = (todo) => {
    if(todo.length > 0) {
      this.state.model.add(todo);
      this.setState((state, props) => ({
        model: state.model,
        todoInput: ''
      }));
    }
  }

  handleKeyDownAddTodo = (e) => {
    if (e.keyCode !== ENTER_KEY) return;
    e.preventDefault();
    this.handleAdd(e.target.value);
  }

  handleRemove = (e) => {
    this.state.model.remove(e.target.parentNode.getAttribute('id'));
    this.setState((state, props) => ({
      model: state.model
    }));
  }

  handleRemoveAll = () => {
    this.state.model.removeAll();
    this.setState((state, props) => ({
      model: state.model,
      todoInput: ''
    }));
  }

  handleCheck = (e) => {
    this.state.model.check(e.target.parentNode.getAttribute('id'));
    this.setState((state, props) => ({
      model: state.model
    }));
  }

  handleFilter = (filter) => {
    this.setState((state, props) => ({
      filter: filter
    }));
  }

  render() {
    const todos = this.state.model.todos || [];
    const todoItems = todos.map((todo, index) => {
      let result = false;

      if(this.state.filter === 'all'){
        result = true;
      } else if(this.state.filter === 'active' && !todo.completed) {
        result = true;
      } else if(this.state.filter === 'completed' && todo.completed) {
        result = true;
      }

      if(result){
        return (
          <TodoItem 
            key={index}
            todo={todo}
            handleRemove={this.handleRemove}
            handleCheck={this.handleCheck}
          />
        );
      }
    });

    return (
      <div>
        <h1>Todolist</h1>
        <TodoFilter filter={this.state.filter} handleFilter={this.handleFilter}/>
        <button onClick={this.handleRemoveAll}>Remove All</button><br />
        <input 
          style={{width: 300}}
          type="text" 
          placeholder="Add"
          onKeyDown={this.handleKeyDownAddTodo}
          onChange={this.handleInput}
          value={this.state.todoInput}
        />
        {todoItems}
      </div>
    );
  }
}

export default TodoList;